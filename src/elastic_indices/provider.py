from datetime import datetime
from typing import Union, Dict

import arrow
from arrow import Arrow
from singleton.singleton import Singleton

from .config import ElasticConfig
from .enum import ElasticIndexSpanEnum, ElasticIndexSpanEnumItem
from .span_config import ElasticIndexSpanConfig
from .exceptions import IndexNotFound


class ElasticIndexDefinition(object):
    """
    Used for getting indices for search, for given entity and date or date range
    """

    def __init__(self, index_prefix, elastic_type, index_span=ElasticIndexSpanEnum.DISABLED):
        self._index_prefix = index_prefix
        self._elastic_type = elastic_type
        if isinstance(index_span, ElasticIndexSpanEnumItem):
            self._index_span = ElasticIndexSpanConfig(ElasticConfig.first_index_date).add_interval_range(index_span)
        elif isinstance(index_span, ElasticIndexSpanConfig):
            self._index_span = index_span
        else:
            raise Exception('index_span has to be one of ElasticIndexSpanEnum or ElasticIndexSpanConfig')

    def get(self, from_date=None, to_date=None):
        """
        Returns list of indices which (by definition) fall under given dates.
        If to_date is not provided, it will default to today
        The goal of this method is to return up to 90 indices (groups) within given date range,
        so if requested entity is spanned by day,
        and date range includes more than 90 days, this method will return monthly index groups and so on.
        The reason is that HTTP GET request has limited URL length
        """
        if not self.is_spanned:
            return [self._index_prefix]

        dates = self._index_span.date_range(from_date, to_date)
        return list(map(lambda d: '{}{}*'.format(self._index_prefix, d), dates))

    def get_one(self, date=None, exact=False):
        """
        Returns only one index which (by definition) falls under given date.
        If date is not provided, it will default to today
        """
        if not self.is_spanned:
            return self._index_prefix
        d = self._index_span.date(date)
        index = '{}{}'.format(self._index_prefix, d)
        return index if exact else index + '*'

    def get_using_interval(self, unit, now=None):
        """
        Returns list of indices which (by definition) fall under given time period.
        Time period (unit) has to be one of: {year|y, month|M, week|w, day|d, hour|h, minute|m, second|s}
        The goal of this method is to return up to 90 indices (groups) within given date range,
        so if requested entity is spanned by day,
        and date range includes more than 90 days, this method will return monthly index groups and so on.
        The reason is that HTTP GET request has limited URL length
        """
        now = now or arrow.utcnow()
        date_from = self._get_beginning_of_interval(unit, now)
        return self.get(from_date=date_from, to_date=now)

    @property
    def index_prefix(self):
        """
        Returns name of the index for entities not spanned by date.
        In case entity is spanned, this method will return an error
        """
        return self._index_prefix

    @property
    def type(self):
        """
        Returns ES document type
        """
        return self._elastic_type

    @property
    def is_spanned(self):
        """
        Returns whether index is spanned or not
        """
        return not self._index_span.is_disabled

    @staticmethod
    def _get_beginning_of_interval(interval, time=None):
        # type: (str, Union[None, Arrow, datetime]) -> Arrow

        try:
            interval_upper = interval.upper()
        except AttributeError:
            raise ValueError('Unexpected type=%s' % type(interval).__name__)
        time = arrow.get(time).to('GMT') if time else arrow.utcnow()
        if interval_upper in ('YEAR', 'Y'):
            frame = 'year'
        elif interval_upper == 'MONTH' or interval == 'M':
            frame = 'month'
        elif interval_upper in ('WEEK', 'W'):
            frame = 'week'
        elif interval_upper in ('DAY', 'D'):
            frame = 'day'
        elif interval_upper in ('HOUR', 'H'):
            frame = 'hour'
        elif interval_upper == 'MINUTE' or interval == 'm':
            frame = 'minute'
        elif interval_upper in ('SECOND', 'S'):
            frame = 'second'
        else:
            raise ValueError('You Must Specify a valid time range.')
        return time.floor(frame)


@Singleton
class ElasticIndexProvider(object):
    """
    This class is used to provide indices for searching different ES entities.
    """

    def __init__(self):
        self._entities = {}  # type: Dict[str, ElasticIndexDefinition]

    def add_index(self, name, definition):
        name = name.upper()
        if self._entities.get(name) is not None:
            raise Exception('Index with name {} already exists'.format(name))
        if not isinstance(definition, ElasticIndexDefinition):
            raise Exception('Index definition has to be instance of ElasticIndexDefinition')
        self._entities[name] = definition
        return self

    def remove_index(self, name):
        self._entities.pop(name.upper(), None)
        return self

    def get(self, name):
        try:
            return self._entities[name.upper()]
        except KeyError:
            raise IndexNotFound(name)

    def get_by_index_prefix(self, prefix):
        try:
            return next(e for e in self._entities.values() if e.index_prefix.startswith(prefix))
        except StopIteration:
            raise IndexNotFound(prefix)

    def __getattr__(self, name):
        return self.get(name)

    @staticmethod
    def set_first_index_date(date):
        ElasticConfig.first_index_date = arrow.get(date).to('GMT').floor('day')
