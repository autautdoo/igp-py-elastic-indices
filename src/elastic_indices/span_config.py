import arrow

from .config import ElasticConfig
from .enum import ElasticIndexSpanEnum, ElasticIndexSpanEnumItem
from .exceptions import IndexDateRangeError


class ElasticIndexSpanConfigRange(object):
    def __init__(self, span_type, from_date, to_date, first_index_date):
        self.span_type = span_type
        self.from_date = arrow.get(from_date or first_index_date)
        self.to_date = arrow.get(to_date) if to_date else None
        if span_type is not ElasticIndexSpanEnum.DISABLED:
            self.from_date = self.from_date.to('GMT').floor(self.span_type.period)
            if self.to_date is not None:
                self.to_date = self.to_date.to('GMT').ceil(self.span_type.period)


class ElasticIndexSpanConfig(object):
    def __init__(self, first_index_date=None):
        self.first_index_date = arrow.get(first_index_date or ElasticConfig.first_index_date).to('GMT').floor('day')
        self.interval_ranges = []

    def add_interval_range(self, span_type, from_date=None, to_date=None):
        if not isinstance(span_type, ElasticIndexSpanEnumItem):
            raise Exception('index_type argument has to be instance of ElasticIndexSpanEnum')
        if from_date and not isinstance(from_date, str) or to_date and not isinstance(to_date, str):
            raise Exception('from_date and to_date have to be date strings')
        self.interval_ranges.append(ElasticIndexSpanConfigRange(span_type, from_date, to_date, self.first_index_date))
        return self

    @property
    def is_disabled(self):
        return len(self.interval_ranges) == 1 and self.interval_ranges[0].span_type is ElasticIndexSpanEnum.DISABLED

    def date(self, date=arrow.utcnow()):
        if not isinstance(date, arrow.arrow.Arrow):
            date = arrow.get(date)
        date = date.to('GMT')
        if not self.interval_ranges:
            raise IndexDateRangeError()
        for r in self.interval_ranges:
            from_date = r.from_date
            to_date = r.to_date or arrow.utcnow()
            if from_date <= date <= to_date:
                return date.format(r.span_type.date_format)
        # range not found. use last one
        r = self.interval_ranges[-1]
        return date.format(r.span_type.date_format)

    def date_range(self, from_date=None, to_date=arrow.utcnow()):
        if self.is_disabled:
            return []
        if from_date is None:
            from_date = self.first_index_date
        if not isinstance(from_date, arrow.arrow.Arrow):
            from_date = arrow.get(from_date)
        if not isinstance(to_date, arrow.arrow.Arrow):
            to_date = arrow.get(to_date)

        from_date = from_date.to('GMT').floor('day')
        to_date = to_date.to('GMT').ceil('day')
        dates = set()

        for r in self.interval_ranges:
            from_date_interval_limit = r.from_date
            to_date_interval_limit = r.to_date or arrow.utcnow().ceil('day')
            if from_date_interval_limit > to_date or to_date_interval_limit < from_date:
                # span out of range. skipping it
                continue

            from_date_used = from_date
            to_date_used = to_date
            if from_date < from_date_interval_limit:
                # in case from_date specified is lower than interval limit, use interval FROM date instead
                from_date_used = from_date_interval_limit
            if to_date > to_date_interval_limit:
                # in case to_date specified is greater than interval limit, use interval TO date instead
                to_date_used = to_date_interval_limit

            if to_date_used < from_date_used:
                # if after manipulations with from_date and to_date to_date becomes lower than from_date
                # find first date in some other interval which will be > from_date
                try:
                    to_date_used = next(r for r in self.interval_ranges if from_date_used < r.from_date).from_date
                    # deduct 1 day, month or year, to not conflict with from_date from next interval
                    to_date_used = to_date_used.shift(**{r.span_type.period_plural: -1})
                except StopIteration:
                    to_date_used = to_date

            # find correct date range - start for default for that index and date range
            # if there are more than 90 indices in that range, go to next range
            # until you find date range with less than 90 indices
            # this is important step because of GET request URL's length
            range_span = r.span_type
            while range_span and range_span.length(to_date_used - from_date_used) >= 90:
                range_span = range_span.next

            from_date_used = from_date_used.floor(range_span.period)
            to_date_used = to_date_used.ceil(range_span.period)
            for date in arrow.Arrow.range(range_span.period, from_date_used, to_date_used):
                dates.add(date.format(range_span.date_format))

        return list(dates)
