class ElasticIndexSpanEnumItem(object):
    def __init__(self, period=None, date_format=None, length_calculation=None, next_enum=None):
        self.period = period
        self.date_format = date_format
        self.length_calculation = length_calculation or (lambda x: 0)
        self.next = next_enum

    @property
    def period_plural(self):
        return self.period + 's'

    def length(self, period):
        """
        Returns how many periods are in given timedelta object
        :param period:
        :return:
        """
        return self.length_calculation(period)


# create enum items
year = ElasticIndexSpanEnumItem('year', 'YYYY', lambda x: x.days / 365.)
month = ElasticIndexSpanEnumItem('month', 'YYYY-MM', lambda x: x.days / 30., year)
day = ElasticIndexSpanEnumItem('day', 'YYYY-MM-DD', lambda x: x.days, month)


# add enumeration items to class
class ElasticIndexSpanEnum(object):
    DISABLED = ElasticIndexSpanEnumItem()
    DAY = day
    MONTH = month
    YEAR = year
