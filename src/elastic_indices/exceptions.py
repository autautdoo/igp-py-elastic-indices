class IndexDateRangeError(Exception):
    def __init__(self):
        super(IndexDateRangeError, self).__init__("Got date which is not in specified date range index configuration")


class IndexNotFound(Exception):
    def __init__(self, name):
        super(IndexNotFound, self).__init__("Index with following name/prefix not found: {}".format(name))
